/*
*				This is my shapes code.  I wanted to use this actor to display at first a triangle, and then any sort of
*				wacky shape I could come up with.  I followed the same pattern as my Cube header and cpp file but I cannot figure
*				out what I'm missing.  I tried looking up new tutorials online but there's literally no help available. (I found
*				one tutorial but it was from someone in MIT and I didn't understand their code at all.  No point in using code I
*				can't understand (yet).)
*				So I think I might know what my issue is, but I'm not sure how to solve it.  A cube has 8 sides, with each side
*				having 4 verticies.  The triangle that I'm trying to create has 5 sides, but 2 sides only have 3 verticies and
*				the others have 4.  I can't use the same exact code for the 3 verticied sides and 4 verticied sides but I don't know
*				how else to go about creating this (was supposed to be) simple shape.  For the time being, I'm going to comment out
*				this code so I don't affect the rest of my program.  I will come back to it whenever I can.
*				Plus side: With how many times I've retyped up this assignment (way too many times.... UE kept crashing to the point
*				of no return, so I had to re-read the tutorials over and over again until I managed to get this program to stay
*				up and running) I feel like I have a good grasp of what I'm doing.  I'm just waiting for that one 'click' for
*				everything to make complete and perfect sense.
*
*
*				WEEK FOUR (STEPPING STONE 3) UPDATE: I attempted to create a 3D pyramid, thinking it would be simple because there
*				are only 5 points total that I'd have to deal with and I'm getting errors.  I'm having issues with my "TriangleIndexCount"
*				and "TangentSetup", along with issues from the "FProcMeshTangent" and converting "int32" to "FVector".  I have no idea
*				what I'm doing wrong and the internet still isn't any help. There's always next week, right?
*
*
*				WEEK FIVE (PRE-STEPPING STONE 4) UPDATE: I finally figured it all out! I was making everything overly complicated! I had
*				to go back to making a 2D triangle and build from there.  I had too many FVectors, which is what was throwing off the
*				TriangleIndexCount and TangentSetup components.  I had to change around my DefinedShape orders, but for the most part
*				I just had to change all of my code from the 5s to 3s and go from there.  I even made a mini triangle to help me
*				reference which point I was trying to use.  Finally! A shape that wasn't a cube!
*
*						**********************************	WEEK SIX - FINAL SUBMISSION **********************************
*
*						Class Purpose: These steps are used for the player to walk up and launch themselves off of by 
*						jumping. This adds some fun to the level because enemy players can attack the player who is
*						in the air and vice versa.
*
*						I used linear algebra to display this shape. To create the shape, I had to 
*						determine where the vertices and tangents were and I used math to figure out the normals. Also,
*						the material and size of the steps can be changed in the editor. 
*/


#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"

#include "Steps.generated.h"


UCLASS()
class PAINTBALL_FIASCO_API ASteps : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASteps();

	// Controls the size of the steps
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Parameters")
		FVector CubeRadius = FVector(100.f, 100.f, 100.f);

protected:
	// Needed to display a visual component
	UPROPERTY(VisibleAnywhere)
		USceneComponent* ThisScene;

	// Displayes the step's mesh
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisMesh; 

	// Makes the steps visible when spawned/dragged into the world
	virtual void PostActorCreated() override;

	// Makes the steps visible whenever the level loads
	virtual void PostLoad() override;

	// Declares the step's mesh form
	void GenerateMesh();

private:
	// Defines the different points of the mesh, tells UE how to apply static meshes to steps
	TArray<FVector>Vertices;

	// Buffer (address) points to vertices, saves memory
	TArray<int32>Triangles;

	// Tells UE which way the surface of the steps are facing
	TArray<FVector>Normals;

	// Perpendicular angle to the normal (90 degrees)
	TArray<FProcMeshTangent>Tangents;

	// Lets 3D renderer know how to apply the meshes and what kind of pattern the texture should have
	TArray<FVector2D>UVs;

	// UNDETERMINED - Have not seen a single color change (allegedly it's the vertices that changes colors)
	TArray<FLinearColor>Colors;

	// Triangle shape
	void AddTriangleMesh(FVector A, FVector B, FVector C, int32& TriIndex, FProcMeshTangent Tangent);

};