/*
**********************************	WEEK SIX - FINAL SUBMISSION **********************************
*
*						Class Purpose : These steps are used for the player to walk up and launch themselves off of by
*						jumping.This adds some fun to the level because enemy players can attack the player who is
*						in the air and vice versa.
*
*						Objects and Actors : I used linear algebra to display this shape.To create the shape, I had to
*						determine where the vertices and tangents were and I used math to figure out the normals.Also,
*						the material and size of the steps can be changed in the editor. Also, I used a material/shader
*						to dynamically change the colors of THESE steps during runtime. I wanted to add some pizzazz
*						to the steps and thought the color change would add a nice touch.
*/


#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "Steps2.generated.h"

UCLASS()
class PAINTBALL_FIASCO_API ASteps2 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASteps2();

	virtual void PostInitializeComponents() override;

	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Parameters")
		FVector StepRadius = FVector(100.f, 100.f, 100.f);

	// Can set the materials in the editor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Parameters")
		UMaterialInterface* MaterialA;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Parameters")
		UMaterialInterface* MaterialB;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Parameters")
		UMaterialInstanceDynamic* DynamicMaterialInstance;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:
	// Needed to display a visual component
	UPROPERTY(VisibleAnywhere)
		USceneComponent* ThisScene;

	// Displays the step's mesh
	UPROPERTY(VisibleAnywhere)
		URuntimeMeshComponent* ThisMesh;

	// Adds a box collision to the mesh
	UPROPERTY(VisibleAnywhere)
		UBoxComponent* ThisCollision;

	// Makes the steps visible when spawned/dragged into the world
	virtual void PostActorCreated() override;

	// Makes the steps visible whenever the level loads
	virtual void PostLoad() override;

	// Declares the step's mesh form
	void GenerateMesh();

private:
	// Defines the different points of the mesh, tells UE how to apply static meshes to steps
	TArray<FVector>Vertices;

	// Buffer (address) points to vertices, saves memory
	TArray<int32>Triangles;

	// Tells UE which way the surface of the steps are facing
	TArray<FVector>Normals;

	// Perpendicular angle to the normal (90 degrees)
	TArray<FRuntimeMeshTangent>Tangents;

	// Lets 3D renderer know how to apply the meshes and what kind of pattern the texture should have
	TArray<FVector2D>UVs;

	// Supposed to change the vertices colors
	TArray<FColor>Colors;

	// Triangle shape
	void AddTriangleMesh(FVector A, FVector B, FVector C, int32& TriIndex, FRuntimeMeshTangent tangent);

	// Activates when the player walks over the box collision of the mesh
	virtual void NotifyActorBeginOverlap(AActor* OtherActor);

	// Ends overlap (material reverts back to original)
	virtual void NotifyActorEndOverlap(AActor* OtherActor);

	
};
