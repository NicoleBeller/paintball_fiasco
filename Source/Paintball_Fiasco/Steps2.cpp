// Fill out your copyright notice in the Description page of Project Settings.

#include "Steps2.h"


// Sets default values
ASteps2::ASteps2()
{
	// Makes the scene component a root component
	ThisScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = ThisScene;

	// Attaches the RuntimeMeshComponent to the new root component
	ThisMesh = CreateDefaultSubobject<URuntimeMeshComponent>(TEXT("GeneratedMesh"));
	ThisMesh->SetupAttachment(RootComponent);

	// Adds the box collision to change the material
	ThisCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	ThisCollision->InitBoxExtent(StepRadius * 1.1f);
	ThisCollision->SetupAttachment(RootComponent);
	ThisCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ThisCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	ThisCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	// Sets the material at creation, runtime can change it
	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialAsset(TEXT("/Game/Materials/m_Red.m_Red"));
	if (MaterialAsset.Succeeded())
	{
		MaterialA = MaterialAsset.Object;
	}

	// Sets the material at creation, runtime can change it
	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialAssetB(TEXT("/Game/Materials/m_Blue.m_Blue"));
	if (MaterialAssetB.Succeeded())
	{
		MaterialB = MaterialAssetB.Object;
	}

}

// This is called when the steps are spawned or clicked and dragged into the world
void ASteps2::PostActorCreated()
{
	// Calls method back to the original owner
	Super::PostActorCreated();
	GenerateMesh();
}

// This is called when the steps are already in the level and the map is open
void ASteps2::PostLoad()
{
	// Calls method back to the original owner
	Super::PostLoad();
	GenerateMesh();
}

// Updates the material within the game world via variables
void ASteps2::GenerateMesh()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	Tangents.Reset();
	UVs.Reset();
	Colors.Reset();

	// Keeps track of the triangles (every shape is made up of triangles)
	int32 TriangleIndexCount = 0;
	FVector DefinedShape[6]; // EDITED: Changed this from 8 (for the cube) to 6 (for the steps)
	FRuntimeMeshTangent TangentSetup;

	// Defines the shape of the steps
	DefinedShape[0] = FVector(-StepRadius.X, StepRadius.Y, StepRadius.Z); // Point 0 (top left)
	DefinedShape[1] = FVector(StepRadius.X, StepRadius.Y, -StepRadius.Z); // Point 1 (left)
	DefinedShape[2] = FVector(-StepRadius.X, StepRadius.Y, -StepRadius.Z); // Point 2 (back left)
	DefinedShape[3] = FVector(StepRadius.X, -StepRadius.Y, -StepRadius.Z); // Point 3 (right)
	DefinedShape[4] = FVector(-StepRadius.X, -StepRadius.Y, -StepRadius.Z); // Point 4 (back right)
	DefinedShape[5] = FVector(-StepRadius.X, -StepRadius.Y, StepRadius.Z); // Point 5 (top right)

	// Passes the defined shape to generate the mesh
	//Front left
	TangentSetup = FRuntimeMeshTangent(0.f, 0.f, 1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[1], DefinedShape[3], TriangleIndexCount, TangentSetup);
	// right
	TangentSetup = FRuntimeMeshTangent(1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[3], DefinedShape[5], TriangleIndexCount, TangentSetup);
	// back left
	TangentSetup = FRuntimeMeshTangent(-1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[5], DefinedShape[4], DefinedShape[2], TriangleIndexCount, TangentSetup);
	// left
	TangentSetup = FRuntimeMeshTangent(1.f, -1.f, -1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[2], DefinedShape[1], TriangleIndexCount, TangentSetup);
	// bottom left
	TangentSetup = FRuntimeMeshTangent(1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[1], DefinedShape[2], DefinedShape[3], TriangleIndexCount, TangentSetup);
	// bottom right
	TangentSetup = FRuntimeMeshTangent(-1.f, -1.f, -1.f);
	AddTriangleMesh(DefinedShape[3], DefinedShape[2], DefinedShape[4], TriangleIndexCount, TangentSetup);
	// front right
	TangentSetup = FRuntimeMeshTangent(0.f, 0.f, -1.f);
	AddTriangleMesh(DefinedShape[5], DefinedShape[3], DefinedShape[4], TriangleIndexCount, TangentSetup);
	// back right
	TangentSetup = FRuntimeMeshTangent(1.f, -1.f, 1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[5], DefinedShape[2], TriangleIndexCount, TangentSetup);

	// Creates the steps and displays them in the game
	ThisMesh->ClearAllMeshSections();
	ThisMesh->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);

	// Applies the material to the object
	ThisMesh->SetMaterial(0, MaterialA);

}

void ASteps2::AddTriangleMesh(FVector A, FVector B, FVector C, int32 & TriIndex, FRuntimeMeshTangent tangent)
{
	// Continuously grows the triangles in the steps shape
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;

	// Adds the vertices to the buffer
	Vertices.Add(A);
	Vertices.Add(B);
	Vertices.Add(C);

	// Adds the triangles to the buffer
	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	// Linear algebra to figure out the normals of the shapes
	FVector ThisNorm = FVector::CrossProduct(A - B, B - C).GetSafeNormal();
	for (int i = 0; i < 3; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(tangent);
		Colors.Add(FColor::Blue);
	}
	UVs.Add(FVector2D(0.f, 1.f));
	UVs.Add(FVector2D(0.f, 0.f));
	UVs.Add(FVector2D(1.f, 1.f));
}

// When the player overlaps the box collision around the step, the material will change
void ASteps2::NotifyActorBeginOverlap(AActor * OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	ThisMesh->SetMaterial(0, MaterialB);
}

// When the player leaves the overlap, the material will revert back to its original color
void ASteps2::NotifyActorEndOverlap(AActor * OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);
	ThisMesh->SetMaterial(0, MaterialA);
}


// not needed since I couldn't implement the fog texture correctly
void ASteps2::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (MaterialA)
	{
		DynamicMaterialInstance = UMaterialInstanceDynamic::Create(MaterialA, this);
	}

	if (DynamicMaterialInstance)
	{
		ThisMesh->SetMaterial(0, DynamicMaterialInstance);
	}
}

#if WITH_EDITOR


void ASteps2::PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FName MemberPropertyChanged = (PropertyChangedEvent.MemberProperty ? PropertyChangedEvent.MemberProperty->GetFName() : NAME_None);

	if ((MemberPropertyChanged == GET_MEMBER_NAME_CHECKED(ASteps2, MaterialA)))
	{
		ThisMesh->SetMaterial(0, MaterialA);
	}
}

#endif