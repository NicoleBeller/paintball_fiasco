// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_FiascoGameMode.h"
#include "Paintball_FiascoHUD.h"
#include "Paintball_FiascoCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintball_FiascoGameMode::APaintball_FiascoGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintball_FiascoHUD::StaticClass();
}
