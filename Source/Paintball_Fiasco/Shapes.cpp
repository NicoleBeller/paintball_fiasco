


#include "Shapes.h"

// Sets default values
AShapes::AShapes()
{
	// Makes the scene component a root component
	ThisScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = ThisScene;

	// Attaches the ProceduralMeshComponent to the new root component
	ThisMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	ThisMesh->SetupAttachment(RootComponent);

}

// This is called when the shape is spawned or clicked and dragged into the world
void AShapes::PostActorCreated()
{
	// Calls method back to the original owner
	Super::PostActorCreated();
	GenerateMesh();
}

// This is called when the shape is already in the level and the map is open
void AShapes::PostLoad()
{
	// Calls method back to the original owner
	Super::PostLoad();
	GenerateMesh();
}

//  Updates the material within the game world via variables
void AShapes::GenerateMesh()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	Tangents.Reset();
	UVs.Reset();
	Colors.Reset();

	// Keeps track of the triangles (every shape is made up of triangles)
	int32 TriangleIndexCount = 0;
	FVector DefinedShape[5]; // EDITED: Changed this from 8 (for the cube) to 5 (for the triangle)
	FProcMeshTangent TangentSetup;

	DefinedShape[0] = FVector(CubeRadius.X, CubeRadius.Y, CubeRadius.Z); // Point 0 (top)
	DefinedShape[1] = FVector(CubeRadius.X, CubeRadius.Y, -CubeRadius.Z); // Point 1 (left)
	DefinedShape[2] = FVector(-CubeRadius.X, CubeRadius.Y, -CubeRadius.Z); // Point 2 (back left)
	DefinedShape[3] = FVector(CubeRadius.X, -CubeRadius.Y, -CubeRadius.Z); // Point 3 (right)
	DefinedShape[4] = FVector(-CubeRadius.X, -CubeRadius.Y, -CubeRadius.Z); // Point 4 (back right)

	// Passes the defined shape to generate the mesh 
	// Front
	TangentSetup = FProcMeshTangent(0.f, 0.f, 0.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[1], DefinedShape[3], TriangleIndexCount, TangentSetup);
	// Right
	TangentSetup = FProcMeshTangent(1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[3], DefinedShape[4], TriangleIndexCount, TangentSetup);
	// Back
	TangentSetup = FProcMeshTangent(-1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[4], DefinedShape[2], TriangleIndexCount, TangentSetup);
	// Left
	TangentSetup = FProcMeshTangent(1.f, -1.f, -1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[2], DefinedShape[1], TriangleIndexCount, TangentSetup);
	// Bottom Left
	TangentSetup = FProcMeshTangent(1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[1], DefinedShape[2], DefinedShape[3], TriangleIndexCount, TangentSetup);
	// Bottom Right
	TangentSetup = FProcMeshTangent(-1.f, -1.f, -1.f);
	AddTriangleMesh(DefinedShape[3], DefinedShape[2], DefinedShape[4], TriangleIndexCount, TangentSetup);

	// Creates the shape
	ThisMesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);
}

void AShapes::AddTriangleMesh(FVector A, FVector B, FVector C, int32 & TriIndex, FProcMeshTangent Tangent)
{
	// Continously grows the triangles in the shape
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;

	// Adds the verticies to the buffer
	Vertices.Add(A);
	Vertices.Add(B);
	Vertices.Add(C);

	// Adds the triangles to the buffer
	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	// Linear algebra to figure out the normals of the shapes
	FVector ThisNorm = FVector::CrossProduct(A - B, B - C).GetSafeNormal();
	for (int i = 0; i < 3; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Green);
	}
	UVs.Add(FVector2D(0.f, 1.f)); // Top Left
	UVs.Add(FVector2D(0.f, 0.f)); // Bottom Left
	UVs.Add(FVector2D(1.f, 1.f)); // Top Right
}

