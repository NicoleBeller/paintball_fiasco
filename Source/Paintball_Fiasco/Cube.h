/*				
*				This is my cube class.  Appropriately named because this class will only ever produce 3D squares, also known as cubes.
*				When I want to create new shapes, I will make new classes.  So for this code, I followed a classmate's tutorial.  I originally followed
*				the tutorial provided from the class but I wanted to see a new way to create 3D shapes.  I actually prefer this method because I 
*				I had an easier time understanding what was going on because the code was spaced out (nothing was commented, though) and I don't
*				think the code was a repetitive as the provided tutorial seemed to be.
*
*						**********************************	WEEK SIX - FINAL SUBMISSION **********************************
*
*						Class Purpose: This was the first shape I created, so it's more of a "Tester baby".  I used the 
*						cube to display the different shaders in the game.  Also, I added the fog shader to some of the
*						cubes and used it as an obstacle for the players to get over.  They're intentionally difficult
*						to see, to sort of slow down the players.  
*
*						This was by far the easier shape to create with code; it also helped that I could find numerous
*						tutorials online specifically for this shape.  Since it was that easy, I tried not to spend as 
*						much time working in/on this class as the others, because I wanted to slightly challenge myself.
*/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Cube.generated.h"

UCLASS()
class PAINTBALL_FIASCO_API ACube : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACube();

	// Controls the size of the cube
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Parameters")
		FVector CubeRadius = FVector(100.f, 100.f, 100.f);

protected:
	// Needed to display a visual component
	UPROPERTY(VisibleAnywhere)
		USceneComponent* ThisScene;

	// Displayes the cube mesh
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisMesh;

	// Makes the cube visible when spawned/dragged into the world
	virtual void PostActorCreated() override;

	// Makes the cube visible whenever the level loads
	virtual void PostLoad() override;

	// Declares the cube's mesh form
	void GenerateMesh();

private:
	// Defines the different points of the mesh, tells UE how to apply static meshes to shapes
	TArray<FVector>Vertices;

	// Buffer (address) points to vertices, saves memory
	TArray<int32>Triangles;

	// Tells UE which way the surface of the shape is facing
	TArray<FVector>Normals;

	// Perpendicular angle to the normal (90 degrees)
	TArray<FProcMeshTangent>Tangents;

	// Lets 3D renderer know how to apply the meshes and what kind of pattern the texture should have
	TArray<FVector2D>UVs;

	// UNDETERMINED - Have not seen a single color change (allegedly it's the vertices that changes colors)
	TArray<FLinearColor>Colors;

	// Triangle shape
	void AddTriangleMesh(FVector TopLeft, FVector BottomLeft, FVector BottomRight, int32& TriIndex, FProcMeshTangent Tangent);

	// Cube shape
	void AddQuadMesh(FVector TopLeft, FVector BottomLeft, FVector TopRight, FVector BottomRight, int32& TriIndex, FProcMeshTangent Tangent);

	
	
};
