/*
*				For this assignment (Stepping Stone 2) we're supposed to code a way to use a fog material (I'm assuming).  I followed
*				the tutorial and the fixes and at this point, I'd like to state that I have not changed a thing yet.  I was always under
*				the impression that with Stepping Stones, you follow them 100% and if you're comfortable enough, then you can do more.
*				I always thought the point of them was to prove that you understood the material and that when you're working on your
*				final assignment, that's when you go above and beyond and change everything.  I guess I always had a backwards way of 
*				viewing them?  At this point, though, I don't feel comfortable at all with understanding what is going on with this code.
*				I honestly don't even see the point of it... I guess it's to have more control over the fog and the shape of it, which
*				would be cool if the code actually worked.  I tried adding the fog code to the player controller, just to see how the
*				code is supposed to work, but of course it didn't do anything, so then I made a new actor just for the fog and again,
*				it just didn't work.  I don't understand the point in being given broken code; I guess I am a go-all-out sort of person.
*				I did add the fog material to the Cube actors, just so I could see a change in something for this project.  I want to 
*				actually do something in this class and I feel like I'm repeatedly hitting a wall for some reason.
*
**						**********************************	WEEK SIX - FINAL SUBMISSION **********************************
*
*						Class Purpose: I know that as a shader, this class should have been used to add actual fog to the
*						game.  I could probably figure it all out if I had more time to do so, but I don't, so I used this
*						shader as a material instead. I added it to some of my cubes, that I placed on the ground, in the
*						way of the player.  It's hard to see, which adds a perfect 'distraction' and can even help out
*						the other team,  if they're quick enough on catching a confused player.
*/

#pragma once

#include "CoreMinimal.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Fog.generated.h"

UCLASS()
class PAINTBALL_FIASCO_API AFog : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFog();

	// Allows actors to initialize themselves in C++
	virtual void PostInitializeComponents() override;

	// Set the fog size
	void setSize(float);

	// Reveal a portion of the fog
	void revealSmoothCircle(const FVector2D & pos, float radius);

private:
	// Propogate the memory's array to the texture
	void UpdateTextureRegions(UTexture2D* Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D* Regions, uint32 SrcPitch, uint32 SrcBpp, uint8* SrcData, bool bFreeData);

	// Fog texture size
	static const int m_textureSize = 512;

	// Creates a planar mesh
	UPROPERTY()
		UStaticMeshComponent* m_squarePlane;

	// Creates a dynamic texture
	UPROPERTY()
		UTexture2D* m_dynamicTexture;

	// Creates a dynamic material
	UPROPERTY()
		UMaterialInterface* m_dynamicMaterial;

	UPROPERTY()
		UMaterialInstanceDynamic* m_dynamicMaterialInstance;

	// Propogate's the memory's array to the texture
	uint8 m_pixelArray[m_textureSize* m_textureSize];

	FUpdateTextureRegion2D m_wholeTextureRegion;
	float m_coverSize;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
