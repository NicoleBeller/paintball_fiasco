// Fill out your copyright notice in the Description page of Project Settings.


#include "RustShape.h"
#include "DrawDebugHelpers.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ARustShape::ARustShape()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Sets the root component
	ShapeRoot = CreateDefaultSubobject<USceneComponent>(TEXT("ShapeRoot"));
	RootComponent = ShapeRoot;

	// Transforms the mesh
	ShapeMesh = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("ShapeMesh"));
	ShapeMesh->AttachToComponent(ShapeRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	// Creates the box collision (trigger) and sets it so that when the player triggers it, the object begins to rust, but when the player leaves, the object reverts back
	ShapeTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ShapeTrigger"));
	ShapeTrigger->bGenerateOverlapEvents = true;
	ShapeTrigger->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	ShapeTrigger->OnComponentBeginOverlap.AddDynamic(this, &ARustShape::OnPlayerTriggerRust);
	ShapeTrigger->OnComponentEndOverlap.AddDynamic(this, &ARustShape::OnPlayerExitRust);
	ShapeTrigger->AttachToComponent(ShapeRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	// 1.0 = no rust; 0.0 = maximum rust
	RustAmount = 1.0f;

}

// Called when the game starts or when spawned
void ARustShape::BeginPlay()
{
	Super::BeginPlay();
	
}

// When the player overlaps the trigger box, the rust sets in
void ARustShape::OnPlayerTriggerRust(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	bRustEffectTriggered = true;
}

// When the player leaves the trigger box, the rust goes away
void ARustShape::OnPlayerExitRust(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	bRustEffectTriggered = false;
}

// Called every frame
void ARustShape::Tick(float DeltaTime)
{
	// Dictates how fast the rust sets in/disappers
	Super::Tick(DeltaTime);
	if (bRustEffectTriggered)
	{
		if (RustAmount > 0.0f)
		{
			RustAmount -= DeltaTime;
		}
	}

	if (!bRustEffectTriggered)
	{
		if (RustAmount < 1.0f)
		{
			RustAmount += DeltaTime;
		}
	}
	
	UMaterialInstanceDynamic* RustMaterialInstance = ShapeMesh->CreateDynamicMaterialInstance(0);

	if (RustMaterialInstance != nullptr)
	{
		RustMaterialInstance->SetScalarParameterValue(FName("RustAmount"), RustAmount);
	}

}