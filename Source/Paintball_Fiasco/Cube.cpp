


#include "Cube.h"


// Sets default values
ACube::ACube()
{
	// Makes the scene component a root component
	ThisScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = ThisScene;

	// Attaches the ProceduralMeshComponent to the new root component
	ThisMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	ThisMesh->SetupAttachment(RootComponent);

}

// This is called when the cube is spawned or clicked and dragged into the world
void ACube::PostActorCreated()
{
	// Calls method back to the original owner
	Super::PostActorCreated();
	GenerateMesh();
}

// This is called when the cube is already in the level and the map is open
void ACube::PostLoad()
{
	// Calls method back to the original owner
	Super::PostLoad();
	GenerateMesh();
}

//  Updates the material within the game world via variables
void ACube::GenerateMesh()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	Tangents.Reset();
	UVs.Reset();
	Colors.Reset();

	// Keeps track of the triangles (every shape is made up of triangles)
	int32 TriangleIndexCount = 0;
	FVector DefinedShape[8];
	FProcMeshTangent TangentSetup;

	// Defines the shape
	DefinedShape[0] = FVector(CubeRadius.X, CubeRadius.Y, CubeRadius.Z); // Forward Top Right
	DefinedShape[1] = FVector(CubeRadius.X, CubeRadius.Y, -CubeRadius.Z); // Forward Bottom Right
	DefinedShape[2] = FVector(CubeRadius.X, -CubeRadius.Y, CubeRadius.Z); // Forward Top Left
	DefinedShape[3] = FVector(CubeRadius.X, -CubeRadius.Y, -CubeRadius.Z); // Forward Bottom Left
	DefinedShape[4] = FVector(-CubeRadius.X, -CubeRadius.Y, CubeRadius.Z); // Forward Top Right
	DefinedShape[5] = FVector(-CubeRadius.X, -CubeRadius.Y, -CubeRadius.Z); // Forward Bottom Right
	DefinedShape[6] = FVector(-CubeRadius.X, CubeRadius.Y, CubeRadius.Z); // Forward Top Left
	DefinedShape[7] = FVector(-CubeRadius.X, CubeRadius.Y, -CubeRadius.Z); // Forward Bottom Left

	// Passes the defined shape to generate the mesh
	// Front
	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(DefinedShape[0], DefinedShape[1], DefinedShape[2], DefinedShape[3], TriangleIndexCount, TangentSetup);
	// Left
	TangentSetup = FProcMeshTangent(1.f, 0.f, 0.f);
	AddQuadMesh(DefinedShape[2], DefinedShape[3], DefinedShape[4], DefinedShape[5], TriangleIndexCount, TangentSetup);
	// Back
	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(DefinedShape[4], DefinedShape[5], DefinedShape[6], DefinedShape[7], TriangleIndexCount, TangentSetup);
	// Right
	TangentSetup = FProcMeshTangent(-1.f, 0.f, 0.f);
	AddQuadMesh(DefinedShape[6], DefinedShape[7], DefinedShape[0], DefinedShape[1], TriangleIndexCount, TangentSetup);
	// Top
	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(DefinedShape[6], DefinedShape[0], DefinedShape[4], DefinedShape[2], TriangleIndexCount, TangentSetup);
	// Bottom
	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(DefinedShape[1], DefinedShape[7], DefinedShape[3], DefinedShape[5], TriangleIndexCount, TangentSetup);

	// Creates the shape
	ThisMesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);
}

// Triangle
void ACube::AddTriangleMesh(FVector TopRight, FVector BottomRight, FVector BottomLeft, int32 & TriIndex, FProcMeshTangent Tangent)
{
	// Continuously grows the triangle shape
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;

	// Adds the verticies to the buffer
	Vertices.Add(TopRight);
	Vertices.Add(BottomRight);
	Vertices.Add(BottomLeft);

	// Adds the triangles to the buffer
	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	// Linear algebra to figure out the normals of the shapes
	FVector ThisNorm = FVector::CrossProduct(TopRight, BottomRight).GetSafeNormal();
	for (int i = 0; i < 3; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Green);
	}
	// Texture coordinate for the top right coordinate
	UVs.Add(FVector2D(0.f, 1.f));
	// Texture coordinate for the bottom right coordinate
	UVs.Add(FVector2D(0.f, 0.f));
	// Texture coordinate for the bottom left coordinate
	UVs.Add(FVector2D(1.f, 0.f));
}

// Cube
void ACube::AddQuadMesh(FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32 & TriIndex, FProcMeshTangent Tangent)
{
	// Continously grows the triangles in the cube shape
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;
	int32 Point4 = TriIndex++;

	// Adds the verticies to the buffer
	Vertices.Add(TopRight);
	Vertices.Add(BottomRight);
	Vertices.Add(TopLeft);
	Vertices.Add(BottomLeft);

	// Adds the triangles to the buffer
	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);
	Triangles.Add(Point4);
	Triangles.Add(Point3);
	Triangles.Add(Point2);

	// Linear algebra to figure out the normals of the shapes
	FVector ThisNorm = FVector::CrossProduct(TopLeft - BottomRight, BottomRight - TopRight).GetSafeNormal();
	for (int i = 0; i < 4; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Green);
	}
	UVs.Add(FVector2D(0.f, 1.f)); // Top Left
	UVs.Add(FVector2D(0.f, 0.f)); // Bottom Left
	UVs.Add(FVector2D(1.f, 1.f)); // Top Right
	UVs.Add(FVector2D(1.f, 0.f)); // Bottom Right
}
