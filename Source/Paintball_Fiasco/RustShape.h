/*
*						**********************************	WEEK SIX - FINAL SUBMISSION **********************************
*
*						Class Purpose: I felt like I needed to create a shader my game could actually use, so I found a 
*						tutorial online on how to make items 'rust' when you walk over the box collision. I figured this
*						shader would suit my game well because I can turn it into a wall and whenever a player crosses
*						the collision, the material will change, and won't let the player hide there anymore.  
*
*						The tutorial had me use a cube and while I wanted to code my own cube, like I did before, I ran
*						out of time, trying to fulfill the other needs from the rubric.  I used a default cube and added
*						the shader code directly to the cube.  Then I turned the cube into a blueprint and used the BP
*						version in my level.  I placed it right where the player spawns and over by the steps (stairs),
*						making it difficult for the player/s to take shelter anywhere.
*
*
*
*/
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "RustShape.generated.h"

UCLASS()
class PAINTBALL_FIASCO_API ARustShape : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARustShape();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	// Root component for the shape
	UPROPERTY(EditAnywhere)
		USceneComponent* ShapeRoot;

	// Mesh for the shape
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* ShapeMesh;

	// Trigger for the shape
	UPROPERTY(EditAnywhere)
		UBoxComponent* ShapeTrigger;

	// Called when player triggers the rust effect
	UFUNCTION()
		void OnPlayerTriggerRust(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Called when player untriggers the rust effect
	UFUNCTION()
		void OnPlayerExitRust(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:

	// Checks to see if rust is in effect or not
	bool bRustEffectTriggered;

	// Dictates how rusty the shape becomes
	float RustAmount;
	
};
