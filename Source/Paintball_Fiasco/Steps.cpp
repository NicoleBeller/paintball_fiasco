
#include "Steps.h"

// Sets default values
ASteps::ASteps()
{
	// Makes the scene component a root component
	ThisScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = ThisScene;

	// Attaches the ProceduralMeshComponent to the new root component
	ThisMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	ThisMesh->SetupAttachment(RootComponent);

}




// This is called when the steps are spawned or clicked and dragged into the world
void ASteps::PostActorCreated()
{
	// Calls method back to the original owner
	Super::PostActorCreated();
	GenerateMesh();
}

// This is called when the steps are already in the level and the map is open
void ASteps::PostLoad()
{
	// Calls method back to the original owner
	Super::PostLoad();
	GenerateMesh();
}

//  Updates the material within the game world via variables
void ASteps::GenerateMesh()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	Tangents.Reset();
	UVs.Reset();
	Colors.Reset();

	// Keeps track of the triangles (every shape is made up of triangles)
	int32 TriangleIndexCount = 0;
	FVector DefinedShape[6]; // EDITED: Changed this from 8 (for the cube) to 6 (for the steps)
	FProcMeshTangent TangentSetup; 

	// Defines the shape of the steps
	DefinedShape[0] = FVector(-CubeRadius.X, CubeRadius.Y, CubeRadius.Z); // Point 0 (top left)
	DefinedShape[1] = FVector(CubeRadius.X, CubeRadius.Y, -CubeRadius.Z); // Point 1 (left)
	DefinedShape[2] = FVector(-CubeRadius.X, CubeRadius.Y, -CubeRadius.Z); // Point 2 (back left)
	DefinedShape[3] = FVector(CubeRadius.X, -CubeRadius.Y, -CubeRadius.Z); // Point 3 (right)
	DefinedShape[4] = FVector(-CubeRadius.X, -CubeRadius.Y, -CubeRadius.Z); // Point 4 (back right)
	DefinedShape[5] = FVector(-CubeRadius.X, -CubeRadius.Y, CubeRadius.Z); // Point 5 (top right)

	// Passes the defined shape to generate the mesh 
	// Front Left
	TangentSetup = FProcMeshTangent(0.f, 0.f, 1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[1], DefinedShape[3], TriangleIndexCount, TangentSetup);
	// Right 
	TangentSetup = FProcMeshTangent(1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[3], DefinedShape[5], TriangleIndexCount, TangentSetup);
	// Back Left
	TangentSetup = FProcMeshTangent(-1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[5], DefinedShape[4], DefinedShape[2], TriangleIndexCount, TangentSetup);
	// Left
	TangentSetup = FProcMeshTangent(1.f, -1.f, -1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[2], DefinedShape[1], TriangleIndexCount, TangentSetup);
	// Bottom Left
	TangentSetup = FProcMeshTangent(1.f, 1.f, -1.f);
	AddTriangleMesh(DefinedShape[1], DefinedShape[2], DefinedShape[3], TriangleIndexCount, TangentSetup);
	// Bottom Right
	TangentSetup = FProcMeshTangent(-1.f, -1.f, -1.f);
	AddTriangleMesh(DefinedShape[3], DefinedShape[2], DefinedShape[4], TriangleIndexCount, TangentSetup);
	// Front Right
	TangentSetup = FProcMeshTangent(0.f, 0.f, -1.f);
	AddTriangleMesh(DefinedShape[5], DefinedShape[3], DefinedShape[4], TriangleIndexCount, TangentSetup);
	// Back Right
	TangentSetup = FProcMeshTangent(1.f, -1.f, 1.f);
	AddTriangleMesh(DefinedShape[0], DefinedShape[5], DefinedShape[2], TriangleIndexCount, TangentSetup);

	// Creates the steps and displays them in the game
	ThisMesh->ClearAllMeshSections();
	ThisMesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);
}



void ASteps::AddTriangleMesh(FVector A, FVector B, FVector C, int32 & TriIndex, FProcMeshTangent Tangent)
{
	// Continously grows the triangles in the shape
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;

	// Adds the verticies to the buffer
	Vertices.Add(A);
	Vertices.Add(B);
	Vertices.Add(C);

	// Adds the triangles to the buffer
	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	// Linear algebra to figure out the normals of the shapes
	FVector ThisNorm = FVector::CrossProduct(A - B, B - C).GetSafeNormal();
	for (int i = 0; i < 3; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Blue);
	}
	UVs.Add(FVector2D(0.f, 1.f)); // Top Left
	UVs.Add(FVector2D(0.f, 0.f)); // Bottom Left
	UVs.Add(FVector2D(1.f, 1.f)); // Top Right
}