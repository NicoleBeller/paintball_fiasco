// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Steps2.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSteps2() {}
// Cross Module References
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_ASteps2_NoRegister();
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_ASteps2();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Fiasco();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	RUNTIMEMESHCOMPONENT_API UClass* Z_Construct_UClass_URuntimeMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ASteps2::StaticRegisterNativesASteps2()
	{
	}
	UClass* Z_Construct_UClass_ASteps2_NoRegister()
	{
		return ASteps2::StaticClass();
	}
	UClass* Z_Construct_UClass_ASteps2()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Fiasco,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Steps2.h" },
				{ "ModuleRelativePath", "Steps2.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisCollision_MetaData[] = {
				{ "Category", "Steps2" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Steps2.h" },
				{ "ToolTip", "Adds a box collision to the mesh" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisCollision = { UE4CodeGen_Private::EPropertyClass::Object, "ThisCollision", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ASteps2, ThisCollision), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(NewProp_ThisCollision_MetaData, ARRAY_COUNT(NewProp_ThisCollision_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisMesh_MetaData[] = {
				{ "Category", "Steps2" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Steps2.h" },
				{ "ToolTip", "Displays the step's mesh" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisMesh = { UE4CodeGen_Private::EPropertyClass::Object, "ThisMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ASteps2, ThisMesh), Z_Construct_UClass_URuntimeMeshComponent_NoRegister, METADATA_PARAMS(NewProp_ThisMesh_MetaData, ARRAY_COUNT(NewProp_ThisMesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisScene_MetaData[] = {
				{ "Category", "Steps2" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Steps2.h" },
				{ "ToolTip", "Needed to display a visual component" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisScene = { UE4CodeGen_Private::EPropertyClass::Object, "ThisScene", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ASteps2, ThisScene), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(NewProp_ThisScene_MetaData, ARRAY_COUNT(NewProp_ThisScene_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMaterialInstance_MetaData[] = {
				{ "Category", "Mesh Parameters" },
				{ "ModuleRelativePath", "Steps2.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMaterialInstance = { UE4CodeGen_Private::EPropertyClass::Object, "DynamicMaterialInstance", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASteps2, DynamicMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(NewProp_DynamicMaterialInstance_MetaData, ARRAY_COUNT(NewProp_DynamicMaterialInstance_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialB_MetaData[] = {
				{ "Category", "Mesh Parameters" },
				{ "ModuleRelativePath", "Steps2.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialB = { UE4CodeGen_Private::EPropertyClass::Object, "MaterialB", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASteps2, MaterialB), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(NewProp_MaterialB_MetaData, ARRAY_COUNT(NewProp_MaterialB_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialA_MetaData[] = {
				{ "Category", "Mesh Parameters" },
				{ "ModuleRelativePath", "Steps2.h" },
				{ "ToolTip", "Can set the materials in the editor" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialA = { UE4CodeGen_Private::EPropertyClass::Object, "MaterialA", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASteps2, MaterialA), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(NewProp_MaterialA_MetaData, ARRAY_COUNT(NewProp_MaterialA_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StepRadius_MetaData[] = {
				{ "Category", "Mesh Parameters" },
				{ "ModuleRelativePath", "Steps2.h" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_StepRadius = { UE4CodeGen_Private::EPropertyClass::Struct, "StepRadius", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASteps2, StepRadius), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_StepRadius_MetaData, ARRAY_COUNT(NewProp_StepRadius_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisCollision,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisMesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisScene,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_DynamicMaterialInstance,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_MaterialB,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_MaterialA,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_StepRadius,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ASteps2>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ASteps2::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASteps2, 4147808391);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASteps2(Z_Construct_UClass_ASteps2, &ASteps2::StaticClass, TEXT("/Script/Paintball_Fiasco"), TEXT("ASteps2"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASteps2);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
