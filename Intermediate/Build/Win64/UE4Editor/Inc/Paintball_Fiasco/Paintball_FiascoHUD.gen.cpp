// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Paintball_FiascoHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintball_FiascoHUD() {}
// Cross Module References
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_APaintball_FiascoHUD_NoRegister();
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_APaintball_FiascoHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Paintball_Fiasco();
// End Cross Module References
	void APaintball_FiascoHUD::StaticRegisterNativesAPaintball_FiascoHUD()
	{
	}
	UClass* Z_Construct_UClass_APaintball_FiascoHUD_NoRegister()
	{
		return APaintball_FiascoHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_APaintball_FiascoHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Fiasco,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "Paintball_FiascoHUD.h" },
				{ "ModuleRelativePath", "Paintball_FiascoHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APaintball_FiascoHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APaintball_FiascoHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintball_FiascoHUD, 2542580768);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintball_FiascoHUD(Z_Construct_UClass_APaintball_FiascoHUD, &APaintball_FiascoHUD::StaticClass, TEXT("/Script/Paintball_Fiasco"), TEXT("APaintball_FiascoHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintball_FiascoHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
