// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Paintball_FiascoGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintball_FiascoGameMode() {}
// Cross Module References
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_APaintball_FiascoGameMode_NoRegister();
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_APaintball_FiascoGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Paintball_Fiasco();
// End Cross Module References
	void APaintball_FiascoGameMode::StaticRegisterNativesAPaintball_FiascoGameMode()
	{
	}
	UClass* Z_Construct_UClass_APaintball_FiascoGameMode_NoRegister()
	{
		return APaintball_FiascoGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_APaintball_FiascoGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Fiasco,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "Paintball_FiascoGameMode.h" },
				{ "ModuleRelativePath", "Paintball_FiascoGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APaintball_FiascoGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APaintball_FiascoGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintball_FiascoGameMode, 785424221);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintball_FiascoGameMode(Z_Construct_UClass_APaintball_FiascoGameMode, &APaintball_FiascoGameMode::StaticClass, TEXT("/Script/Paintball_Fiasco"), TEXT("APaintball_FiascoGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintball_FiascoGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
