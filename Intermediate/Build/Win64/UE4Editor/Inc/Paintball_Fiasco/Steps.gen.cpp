// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Steps.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSteps() {}
// Cross Module References
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_ASteps_NoRegister();
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_ASteps();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Fiasco();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ASteps::StaticRegisterNativesASteps()
	{
	}
	UClass* Z_Construct_UClass_ASteps_NoRegister()
	{
		return ASteps::StaticClass();
	}
	UClass* Z_Construct_UClass_ASteps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Fiasco,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Steps.h" },
				{ "ModuleRelativePath", "Steps.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisMesh_MetaData[] = {
				{ "Category", "Steps" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Steps.h" },
				{ "ToolTip", "Displayes the step's mesh" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisMesh = { UE4CodeGen_Private::EPropertyClass::Object, "ThisMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ASteps, ThisMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_ThisMesh_MetaData, ARRAY_COUNT(NewProp_ThisMesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisScene_MetaData[] = {
				{ "Category", "Steps" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Steps.h" },
				{ "ToolTip", "Needed to display a visual component" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisScene = { UE4CodeGen_Private::EPropertyClass::Object, "ThisScene", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ASteps, ThisScene), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(NewProp_ThisScene_MetaData, ARRAY_COUNT(NewProp_ThisScene_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CubeRadius_MetaData[] = {
				{ "Category", "Mesh Parameters" },
				{ "ModuleRelativePath", "Steps.h" },
				{ "ToolTip", "Controls the size of the steps" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_CubeRadius = { UE4CodeGen_Private::EPropertyClass::Struct, "CubeRadius", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASteps, CubeRadius), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_CubeRadius_MetaData, ARRAY_COUNT(NewProp_CubeRadius_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisMesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisScene,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CubeRadius,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ASteps>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ASteps::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASteps, 1320092872);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASteps(Z_Construct_UClass_ASteps, &ASteps::StaticClass, TEXT("/Script/Paintball_Fiasco"), TEXT("ASteps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASteps);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
