// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_FIASCO_Shapes_generated_h
#error "Shapes.generated.h already included, missing '#pragma once' in Shapes.h"
#endif
#define PAINTBALL_FIASCO_Shapes_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_RPC_WRAPPERS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShapes(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_AShapes(); \
public: \
	DECLARE_CLASS(AShapes, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(AShapes) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_INCLASS \
private: \
	static void StaticRegisterNativesAShapes(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_AShapes(); \
public: \
	DECLARE_CLASS(AShapes, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(AShapes) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShapes(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShapes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShapes); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShapes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShapes(AShapes&&); \
	NO_API AShapes(const AShapes&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShapes(AShapes&&); \
	NO_API AShapes(const AShapes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShapes); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShapes); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AShapes)


#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ThisScene() { return STRUCT_OFFSET(AShapes, ThisScene); } \
	FORCEINLINE static uint32 __PPO__ThisMesh() { return STRUCT_OFFSET(AShapes, ThisMesh); }


#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_52_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h_55_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_Shapes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
