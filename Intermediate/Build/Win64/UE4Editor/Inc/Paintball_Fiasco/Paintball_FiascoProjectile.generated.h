// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef PAINTBALL_FIASCO_Paintball_FiascoProjectile_generated_h
#error "Paintball_FiascoProjectile.generated.h already included, missing '#pragma once' in Paintball_FiascoProjectile.h"
#endif
#define PAINTBALL_FIASCO_Paintball_FiascoProjectile_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_FiascoProjectile(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_APaintball_FiascoProjectile(); \
public: \
	DECLARE_CLASS(APaintball_FiascoProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(APaintball_FiascoProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_FiascoProjectile(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_APaintball_FiascoProjectile(); \
public: \
	DECLARE_CLASS(APaintball_FiascoProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(APaintball_FiascoProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_FiascoProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_FiascoProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_FiascoProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_FiascoProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_FiascoProjectile(APaintball_FiascoProjectile&&); \
	NO_API APaintball_FiascoProjectile(const APaintball_FiascoProjectile&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_FiascoProjectile(APaintball_FiascoProjectile&&); \
	NO_API APaintball_FiascoProjectile(const APaintball_FiascoProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_FiascoProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_FiascoProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_FiascoProjectile)


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(APaintball_FiascoProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(APaintball_FiascoProjectile, ProjectileMovement); } \
	FORCEINLINE static uint32 __PPO__SpawnMaterial() { return STRUCT_OFFSET(APaintball_FiascoProjectile, SpawnMaterial); }


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_9_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
