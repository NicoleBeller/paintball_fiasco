// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_FIASCO_Paintball_FiascoHUD_generated_h
#error "Paintball_FiascoHUD.generated.h already included, missing '#pragma once' in Paintball_FiascoHUD.h"
#endif
#define PAINTBALL_FIASCO_Paintball_FiascoHUD_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_RPC_WRAPPERS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_FiascoHUD(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_APaintball_FiascoHUD(); \
public: \
	DECLARE_CLASS(APaintball_FiascoHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(APaintball_FiascoHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_FiascoHUD(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_APaintball_FiascoHUD(); \
public: \
	DECLARE_CLASS(APaintball_FiascoHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(APaintball_FiascoHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_FiascoHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_FiascoHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_FiascoHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_FiascoHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_FiascoHUD(APaintball_FiascoHUD&&); \
	NO_API APaintball_FiascoHUD(const APaintball_FiascoHUD&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_FiascoHUD(APaintball_FiascoHUD&&); \
	NO_API APaintball_FiascoHUD(const APaintball_FiascoHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_FiascoHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_FiascoHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_FiascoHUD)


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_9_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
