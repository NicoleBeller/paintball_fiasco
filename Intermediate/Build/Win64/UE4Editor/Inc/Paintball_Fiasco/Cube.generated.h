// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_FIASCO_Cube_generated_h
#error "Cube.generated.h already included, missing '#pragma once' in Cube.h"
#endif
#define PAINTBALL_FIASCO_Cube_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_RPC_WRAPPERS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACube(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ACube(); \
public: \
	DECLARE_CLASS(ACube, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ACube) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_INCLASS \
private: \
	static void StaticRegisterNativesACube(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ACube(); \
public: \
	DECLARE_CLASS(ACube, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ACube) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACube(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACube) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACube); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACube); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACube(ACube&&); \
	NO_API ACube(const ACube&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACube(ACube&&); \
	NO_API ACube(const ACube&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACube); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACube); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACube)


#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ThisScene() { return STRUCT_OFFSET(ACube, ThisScene); } \
	FORCEINLINE static uint32 __PPO__ThisMesh() { return STRUCT_OFFSET(ACube, ThisMesh); }


#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_27_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_Cube_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
