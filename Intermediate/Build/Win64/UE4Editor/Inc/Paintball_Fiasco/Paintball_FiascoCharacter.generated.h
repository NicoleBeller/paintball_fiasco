// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_FIASCO_Paintball_FiascoCharacter_generated_h
#error "Paintball_FiascoCharacter.generated.h already included, missing '#pragma once' in Paintball_FiascoCharacter.h"
#endif
#define PAINTBALL_FIASCO_Paintball_FiascoCharacter_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_RPC_WRAPPERS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_FiascoCharacter(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_APaintball_FiascoCharacter(); \
public: \
	DECLARE_CLASS(APaintball_FiascoCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(APaintball_FiascoCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_FiascoCharacter(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_APaintball_FiascoCharacter(); \
public: \
	DECLARE_CLASS(APaintball_FiascoCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(APaintball_FiascoCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_FiascoCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_FiascoCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_FiascoCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_FiascoCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_FiascoCharacter(APaintball_FiascoCharacter&&); \
	NO_API APaintball_FiascoCharacter(const APaintball_FiascoCharacter&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_FiascoCharacter(APaintball_FiascoCharacter&&); \
	NO_API APaintball_FiascoCharacter(const APaintball_FiascoCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_FiascoCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_FiascoCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_FiascoCharacter)


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaintball_FiascoCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaintball_FiascoCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaintball_FiascoCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaintball_FiascoCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaintball_FiascoCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaintball_FiascoCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaintball_FiascoCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaintball_FiascoCharacter, L_MotionController); }


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_25_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_Paintball_FiascoCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
