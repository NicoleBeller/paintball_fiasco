// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_FIASCO_Steps2_generated_h
#error "Steps2.generated.h already included, missing '#pragma once' in Steps2.h"
#endif
#define PAINTBALL_FIASCO_Steps2_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_RPC_WRAPPERS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASteps2(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ASteps2(); \
public: \
	DECLARE_CLASS(ASteps2, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ASteps2) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_INCLASS \
private: \
	static void StaticRegisterNativesASteps2(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ASteps2(); \
public: \
	DECLARE_CLASS(ASteps2, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ASteps2) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASteps2(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASteps2) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASteps2); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASteps2); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASteps2(ASteps2&&); \
	NO_API ASteps2(const ASteps2&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASteps2(ASteps2&&); \
	NO_API ASteps2(const ASteps2&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASteps2); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASteps2); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASteps2)


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ThisScene() { return STRUCT_OFFSET(ASteps2, ThisScene); } \
	FORCEINLINE static uint32 __PPO__ThisMesh() { return STRUCT_OFFSET(ASteps2, ThisMesh); } \
	FORCEINLINE static uint32 __PPO__ThisCollision() { return STRUCT_OFFSET(ASteps2, ThisCollision); }


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_23_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_Steps2_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
