// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Cube.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCube() {}
// Cross Module References
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_ACube_NoRegister();
	PAINTBALL_FIASCO_API UClass* Z_Construct_UClass_ACube();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Fiasco();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ACube::StaticRegisterNativesACube()
	{
	}
	UClass* Z_Construct_UClass_ACube_NoRegister()
	{
		return ACube::StaticClass();
	}
	UClass* Z_Construct_UClass_ACube()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Fiasco,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Cube.h" },
				{ "ModuleRelativePath", "Cube.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisMesh_MetaData[] = {
				{ "Category", "Cube" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Cube.h" },
				{ "ToolTip", "Displayes the cube mesh" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisMesh = { UE4CodeGen_Private::EPropertyClass::Object, "ThisMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ACube, ThisMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_ThisMesh_MetaData, ARRAY_COUNT(NewProp_ThisMesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisScene_MetaData[] = {
				{ "Category", "Cube" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Cube.h" },
				{ "ToolTip", "Needed to display a visual component" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisScene = { UE4CodeGen_Private::EPropertyClass::Object, "ThisScene", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ACube, ThisScene), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(NewProp_ThisScene_MetaData, ARRAY_COUNT(NewProp_ThisScene_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CubeRadius_MetaData[] = {
				{ "Category", "Mesh Parameters" },
				{ "ModuleRelativePath", "Cube.h" },
				{ "ToolTip", "Controls the size of the cube" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_CubeRadius = { UE4CodeGen_Private::EPropertyClass::Struct, "CubeRadius", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACube, CubeRadius), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_CubeRadius_MetaData, ARRAY_COUNT(NewProp_CubeRadius_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisMesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisScene,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CubeRadius,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ACube>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ACube::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACube, 2351284653);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACube(Z_Construct_UClass_ACube, &ACube::StaticClass, TEXT("/Script/Paintball_Fiasco"), TEXT("ACube"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACube);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
