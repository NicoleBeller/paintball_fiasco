// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_FIASCO_Steps_generated_h
#error "Steps.generated.h already included, missing '#pragma once' in Steps.h"
#endif
#define PAINTBALL_FIASCO_Steps_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_RPC_WRAPPERS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASteps(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ASteps(); \
public: \
	DECLARE_CLASS(ASteps, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ASteps) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_INCLASS \
private: \
	static void StaticRegisterNativesASteps(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ASteps(); \
public: \
	DECLARE_CLASS(ASteps, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ASteps) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASteps(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASteps) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASteps); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASteps); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASteps(ASteps&&); \
	NO_API ASteps(const ASteps&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASteps(ASteps&&); \
	NO_API ASteps(const ASteps&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASteps); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASteps); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASteps)


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ThisScene() { return STRUCT_OFFSET(ASteps, ThisScene); } \
	FORCEINLINE static uint32 __PPO__ThisMesh() { return STRUCT_OFFSET(ASteps, ThisMesh); }


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_52_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h_55_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_Steps_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
