// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PAINTBALL_FIASCO_RustShape_generated_h
#error "RustShape.generated.h already included, missing '#pragma once' in RustShape.h"
#endif
#define PAINTBALL_FIASCO_RustShape_generated_h

#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerExitRust) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPlayerExitRust(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPlayerTriggerRust) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPlayerTriggerRust(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerExitRust) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPlayerExitRust(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPlayerTriggerRust) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPlayerTriggerRust(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARustShape(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ARustShape(); \
public: \
	DECLARE_CLASS(ARustShape, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ARustShape) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_INCLASS \
private: \
	static void StaticRegisterNativesARustShape(); \
	friend PAINTBALL_FIASCO_API class UClass* Z_Construct_UClass_ARustShape(); \
public: \
	DECLARE_CLASS(ARustShape, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Fiasco"), NO_API) \
	DECLARE_SERIALIZER(ARustShape) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARustShape(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARustShape) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARustShape); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARustShape); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARustShape(ARustShape&&); \
	NO_API ARustShape(const ARustShape&); \
public:


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARustShape(ARustShape&&); \
	NO_API ARustShape(const ARustShape&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARustShape); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARustShape); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARustShape)


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ShapeRoot() { return STRUCT_OFFSET(ARustShape, ShapeRoot); } \
	FORCEINLINE static uint32 __PPO__ShapeMesh() { return STRUCT_OFFSET(ARustShape, ShapeMesh); } \
	FORCEINLINE static uint32 __PPO__ShapeTrigger() { return STRUCT_OFFSET(ARustShape, ShapeTrigger); }


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_25_PROLOG
#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_RPC_WRAPPERS \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_INCLASS \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_PRIVATE_PROPERTY_OFFSET \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_INCLASS_NO_PURE_DECLS \
	Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Fiasco_Source_Paintball_Fiasco_RustShape_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
