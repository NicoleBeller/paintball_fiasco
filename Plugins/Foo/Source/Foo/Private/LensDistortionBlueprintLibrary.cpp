/*
*				Stepping Stone three was all about shaders.  I went two different routes this week.  One with the provided tutorial, this C++ code
*				and one with the tutorial link provided in the Announcements.  I think both routes are actually pretty cool!! With the C++ code I
*				was able to create an interactive-changeable material.  The player can press the 'w' or 's' key while playing and on the big block
*				wall, the material will change accordingly.  I didn't change any of the code at this moment because I think it's perfect as-is, and
*				honestly, I'm not quite sure what I'd want to change about it; I'm not sure how changing a material while playing a game can be really
*				beneficial to the player or if this will just be senselessly added code.  As for the other version, I can see many things I can do with
*				it.  Once I can get the fog situation under control, I could add 'AI' players with the special cel-shaded material, to add some
*				pizzazz.  Unfortunately, to use that shader, it seems like I have to just use blueprints and not C++ code.
*/


#include "LensDistortionBlueprintLibrary.h"

ULensDistortionBlueprintLibrary::ULensDistortionBlueprintLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{ }

// static
void ULensDistortionBlueprintLibrary::GetUndistortOverscanFactor(
	const FFooCameraModel& CameraModel,
	float DistortedHorizontalFOV,
	float DistortedAspectRatio,
	float& UndistortOverscanFactor)
{
	UndistortOverscanFactor = CameraModel.GetUndistortOverscanFactor(DistortedHorizontalFOV, DistortedAspectRatio);
}

// static
void ULensDistortionBlueprintLibrary::DrawUVDisplacementToRenderTarget(
	const UObject* WorldContextObject,
	const FFooCameraModel& CameraModel,
	float DistortedHorizontalFOV,
	float DistortedAspectRatio,
	float UndistortOverscanFactor,
	class UTextureRenderTarget2D* OutputRenderTarget,
	float OutputMultiply,
	float OutputAdd)
{
	CameraModel.DrawUVDisplacementToRenderTarget(
		WorldContextObject->GetWorld(),
		DistortedHorizontalFOV, DistortedAspectRatio,
		UndistortOverscanFactor, OutputRenderTarget,
		OutputMultiply, OutputAdd);
}
